$(document).ready(function() {

	$(".current").removeAttr('title');
	
	$(window).scroll(function() {

		var y = $(window).scrollTop();
		
		if (y > 30)
		{
			$("header").css(
				{
				"height":"29",
				"border-top":"none",
				"position":"fixed",
				"top":"0",
				"transition":"all .25s linear"
				});	
			$("nav").css({
				"box-shadow":"0 2px 10px #666",
				"background":"#fff",
				"transition":"all .5s linear"
			});
			$('img[src="images/parallaxLogo.png"]').remove();
			$("header").append("<img src = 'images/parallaxLogoSm.png' class='logo'>");
			$(".logo").css({
				"top":"5px",
				"left": "0", 
				"transition":"all .25s linear"
			});
		}
		else
		{
			$("header").css({
				"width": "1000px",
			    "height": "150px",
			    "border-top": "11px solid #ef3e36",
			    "position": "relative",
			    "background": "#fff",
				"transition":"all .25s linear"
			});
			$("nav").css({
				"box-shadow":"none",
				"transition":"all .5s linear",
			});
			$("header").append("<img src = 'images/parallaxLogo.png' class='logo'>");
			$('img[src="images/parallaxLogoSm.png"]').remove();
			$(".logo").css({
				"top":"21px",
				"left":"57px",
				"transition":"all .25s linear",
				"position":"absolute",
			});
		}
	});
	
});
		
	
	
	
	
	