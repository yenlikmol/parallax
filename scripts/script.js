var map;

var one = {
    url: 'images/iconPlant.png',
    anchor: new google.maps.Point(18, 18)
};
var two = {
    url: 'images/iconSpar.png',
    anchor: new google.maps.Point(18, 18)
};
var three = {
    url: 'images/iconRig.png',
    anchor: new google.maps.Point(18, 18)
};
var four = {
    url: 'images/iconRig.png',
    anchor: new google.maps.Point(18, 18)
};

var infoWindow = new google.maps.InfoWindow();

var sites = [
    ["Ojeda Processing Plant", 24.126208, -97.741941, one, '#Ojeda'],
    ["Arana Spar", 25.685376, -94.909758, two, '#Arana'],
    ["Quiroga Rig", 22.277888, -94.787785, three, '#Quiroga'],
    ["Orizba Rig", 23.272791, -92.991155, four, '#Orizba']
];

function setMarkers(map, locations) {
    for (var i = 0; i < locations.length; i++) {
        var p = locations[i];
        var myLatLng = new google.maps.LatLng(p[1], p[2]);
        var contentData = ('<div class="infoWin">' + ('<strong>' + p[0] + '</strong><p>' + p[1] + '° Latitude <br>' + p[2] + '° Longitude</p>' + '<small><u><a href="'+ p[4]+'">More Info</a></u></small>') + '<div>');

		var text = {
		url: locations[i][0],
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(18, 18)
		};
		
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: p[i][0],
            content: contentData,
            icon: p[3]
        });

        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(this.content);
            infowindow.open(map, this);

        });
    }
}

function initialize() {
    var center = new google.maps.LatLng(18.5, -96);
    var mapOptions = {
        zoom: 5,
        center: center
    }
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    setMarkers(map, sites);
}

function calcRoute() {
    var start = document.getElementById('start').value;
    var startComma = start.indexOf(',');
    var startLat = parseFloat(start.substring(0, startComma));
    var startLong = parseFloat(start.substring(startComma + 1, start.length));
    var startPoint = new google.maps.LatLng(startLat, startLong);

    var end = document.getElementById('end').value;
    var endComma = end.indexOf(',');
    var endLat = parseFloat(end.substring(0, endComma));
    var endLong = parseFloat(end.substring(endComma + 1, end.length));
    var endPoint = new google.maps.LatLng(endLat, endLong);

    //calculates distance between two points in km's
    function calcDistance(){
        return Math.round(google.maps.geometry.spherical.computeDistanceBetween(startPoint, endPoint) / 1000);
    }

    document.getElementById("DistanceOut").innerHTML = (calcDistance()) + ' km';

    var route = [startPoint, endPoint];

    // Creating the polyline object
    var polyline = new google.maps.Polyline({
        path: route,
        strokeColor: 'red'
    });

    polyline.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);

